<div class='bg-teal-dark pl-8 py-6 flex flex-row justify-between items-center flex-wrap'>

    <a href="{{ url('/') }}" class="text-xl text-white font-bold link home-link">
        Mealplanner
    </a>

    @if (Route::has('login'))

        <div class="flex flex-row justify-between items-center links flex-wrap pr-8">
            @auth
                <a class="link home-link px-2" href="{{ url('/ingredients') }}" style='padding-left: 0;'>Ingredients</a>
                <a class="link home-link px-2" href="{{ url('/recipes') }}">Recipes</a>
                <a class="link home-link px-2" href="{{ url('/mealplans') }}">MealPlans</a>
                <a class="link home-link px-2" href="{{ route('logout') }}">Logout</a>
            @else
                <a class="link home-link px-2" href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a class="link home-link" href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>

    @endif

</div>
