@extends('layouts.app')
@section('content')
<div class="flex flex-col">
    <h3>Recipes</h3>
    <div>
        @foreach ($recipes as $recipe)
            <div>
                <div>{{ $recipe->name }}</div>
            </div>
        @endforeach
    </div>
</div>
@endsection
