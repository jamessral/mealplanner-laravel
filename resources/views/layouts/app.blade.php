<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Meal Deal</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}" />
        <!-- JS -->
        <script type="text/javascript" href="{{ mix('/js/app.js') }}"></script>
    </head>
    <body>
        <div>
            @include('partials.nav')
        </div>
        <div class="content">
            @yield('content')
        </div>
    </body>
</html>
