@extends('layouts.app')

@section('content')
<div class="form-container">
    <h3 class="my-4">{{ __('Login') }}</h3>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="my-4">
            <label for="email">{{ __('E-Mail Address') }}</label>

            <div>
                <input id="email" type="email" class="form-field{{ $errors->has('email') ? '-error' : '' }}"
                    name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                <span class="text-red text-sm" role="alert">
                    {{ $errors->first('email') }}
                </span>
                @endif
            </div>
        </div>

        <div class="my-4">
            <label for="password">{{ __('Password') }}</label>

            <div>
                <input id="password" type="password" class="form-field{{ $errors->has('password') ? '-error' : '' }}"
                    name="password" required>

                @if ($errors->has('password'))
                <span class="text-red text-sm" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="my-4">
            <input type="checkbox" name="remember" id="remember"
                {{ old('remember') ? 'checked' : '' }}>

            <label for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>

        <div class="mt-8 flex flex-row justify-between items-center">
            @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
            @endif

            <button type="submit" class="btn btn-teal-dark">
                {{ __('Login') }}
            </button>
        </div>
    </form>
</div>
@endsection
