@extends('layouts.app')

@section('content')
<div class="form-container">
    <h3 class="my-4">{{ __('Register') }}</h3>
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="my-4">
            <label for="name">{{ __('Name') }}</label>

            <div>
                <input id="name" type="text" class="form-field {{ $errors->has('name') ? 'form-field-error' : '' }}"
                    name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                <span class="text-red text-sm" role="alert">
                    {{ $errors->first('name') }}
                </span>
                @endif
            </div>
        </div>

        <div class="my-4">
            <label for="email">{{ __('E-Mail Address') }}</label>

            <div>
                <input id="email" type="email" class="form-field {{ $errors->has('email') ? 'form-field-error' : '' }}"
                    name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="text-red text-sm" role="alert">
                    {{ $errors->first('email') }}
                </span>
                @endif
            </div>
        </div>

        <div class="my-4">
            <label for="password">{{ __('Password') }}</label>

            <div>
                <input id="password" type="password" class="form-field {{ $errors->has('password') ? 'form-field-error' : '' }}"
                    name="password" required>

                @if ($errors->has('password'))
                <span class="my-4 text-red text-sm" role="alert">
                    {{ $errors->first('password') }}
                </span>
                @endif
            </div>
        </div>

        <div class="my-4">
            <label for="password-confirm">{{ __('Confirm Password') }}</label>

            <div>
                <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required>
            </div>
        </div>

        <button type="submit" class="btn btn-teal-dark ml-auto flex">
            {{ __('Register') }}
        </button>
    </form>
</div>
@endsection
