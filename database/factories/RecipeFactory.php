<?php

use Faker\Generator as Faker;

$factory->define(App\Recipe::class, function (Faker $faker) {
    return [
        'name' => $faker->word(12),
        'description' => $faker->sentences(3, true),
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
