<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RecipesTest extends DuskTestCase
{
    public function setUp() : void
    {
        parent::setUp();
        factory(\App\User::class, 4)->create();
        factory(\App\Recipe::class, 15)->create();
    }

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $user = \App\User::find(1);
            $recipe = factory(\App\Recipe::class)->create(['user_id' => $user->id]);
            $browser
                ->loginAs($user)
                ->visit('/')
                ->assertSee('Mealplanner')
                ->clickLink('Recipes')
                ->assertSee('Recipes')
                ->assertSee($recipe->name);
        });
    }
}
